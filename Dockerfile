FROM python:3.8-slim-buster
WORKDIR /app
RUN apt update -y
RUN apt install python3-pip -y
COPY . /app
RUN pip install wheel
RUN pip install -r requirements.txt
RUN pip install gunicorn
EXPOSE 8000
CMD ["python","manage.py","runserver"]


